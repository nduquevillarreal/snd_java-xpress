# README #

To run this program you would need to install Java in its latest version and Xpress-Mosel (for short, Mosel) Language.

The main is codes in Java script and automatically runs the Xpress optimizer. 
The layout selection problem is solved using the Xpress-Mosel (for short, Mosel) Language. 
To run this model, the user has the choice between using the Mosel Command Line Interpreter 
or the graphical userinterface Xpress-IVE. To solve this optimization problem we use the 
Xpress-Optimizer mixed integer solver using Xpress-IVE. 
